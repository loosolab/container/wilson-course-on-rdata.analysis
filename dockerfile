FROM docker.gitlab.gwdg.de/loosolab/container/r-data.analysis:r3.6.2_bioc3.10
LABEL maintainer="Mario Looso"
RUN \
sudo -u rstudio Rscript -e 'source("https://gitlab.gwdg.de/loosolab/container/wilson-course-on-rdata.analysis/raw/master/wilsoncourse.R")'

ENV \
ROOT=TRUE \
DISABLE_AUTH=TRUE
